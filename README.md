# Pancake
Postgres database migrations as easy as... Pancake!

**Please note: Pancake is still very early in the development process,
and hasn't been properly tested yet. Please don't use Pancake in
production yet.**

Pancake makes your database schema modifications painless by getting rid
of complicated and messy `01_INITIAL_CHANGES.sql` migration files.
Instead, Pancake generates a single schema file that can be commit to
your VCS and serves as the single source of truth. Any database that
doesn't match your schema is diff'd, and made to conform.

Pancake is designed as a tool for developers to help keep their
databases in order, from local machine through to staging and production.


## Requirements
* Docker
* Postgres 10*

*Pancake uses an internal Postgres database to diff your database
against. Different versions of Postgres may work, but are not
recommended.

## Usage
Pancake is intended to fit seamlessly into your Docker Compose pipeline.
A working [docker-compose.yml](https://gitlab.com/MeikG/Pancake/blob/master/docker-compose.yml)
example can be found in this repository.

Example Docker Compose service:

```yaml
services:
  pancake:
    image: meik/pancake
    volumes:
     - .:/data
    links:
     - database
    environment:
     - POSTGRES_USER=foo
     - POSTGRES_PASSWORD=bar
     - POSTGRES_HOST=database
     - POSTGRES_DB=baz

  database:
    image: postgres:10
    environment:
     - POSTGRES_USER=foo
     - POSTGRES_PASSWORD=bar
     - POSTGRES_DB=baz
    restart: always
```

Pancake must be linked to an existing Postgres 10 database, and
configured with the username, password and db environment variables used
to initialise the database. It also requires `POSTGRES_HOST`, the name
of the linked database. This will start Pancake in **persistent** mode.

For advanced usage, such as altering the mode that Pancake starts in,
add the `command` instruction, with one of the commands below.

```yaml
services:
  pancake:
    ...
    command: watch
    ...
```

Staging environments set-up with the migrate command can be used to
automatically update your databases, although we'd recommend using
`generate` and manually executing the commands yourself.

## Commands
When in persistent mode (default), Pancake won't do anything. Commands
can be issued to Pancake as follows:

```
docker exec -it (Pancake container) (command)
```

Available commands are:
* **generate**

  Pancake will generate a `schema.sql` file that represents your current
  database schema. If your database doesn't match the schema file, it
  also generate a `_migrate.sql` file, that represents the difference
  between the schema and your current database.

  *This command does **not** modify your database in any way, and is safe to run
  on production servers.*

* **migrate**

  Pancake will migrate your database to match the current schema. If no
  `_migrate.sql` file exists, it will generate one. **Please note: this
  should not be run in production.** Migrate will make permanent changes
  to your database and can easily result in data loss.

* **watch**

  Pancake will watch your database at regular intervals (default 60s)
  and generate the schema. The interval can be set as the environment
  variable `WATCH_INTERVAL`.

## Known Issues
* Pancake uses Migra for database diffing, and it appears that it won't
automatically create database schemas. **If you're creating or deleting
database schemas, perform this step manually.**