#!/bin/bash
# Clear out all of our tmp sql files.
rm /tmp/*.sql > /dev/null 2> /dev/null

# Start the postgres service.
service postgresql start > /dev/null 2> /dev/null

# Get the user/group of the data folder.
USER=$(stat -c '%u' /data)
GROUP=$(stat -c '%g' /data)

echo "Running schema generation..."
echo

echo "   Waiting for databases to become ready."
# Wait for services to become ready.
runuser -l postgres -c "pg_isready -h localhost" > /dev/null
runuser -l postgres -c "pg_isready -h $POSTGRES_HOST" > /dev/null
echo "      ⤷ Databases ready."

echo "      ⤷ Configuring local db for schema execution."
# If the user & database exist, drop them.
runuser -l postgres -c "psql -c \"DROP DATABASE IF EXISTS $POSTGRES_DB\"" > /dev/null 2> /dev/null
runuser -l postgres -c "psql -c \"DROP USER IF EXISTS $POSTGRES_USER\"" > /dev/null 2> /dev/null

# Create a new local user.
runuser -l postgres -c "psql -c \"CREATE USER $POSTGRES_USER WITH SUPERUSER PASSWORD '$POSTGRES_PASSWORD'\"" > /dev/null

# Create a database with the latest schema.
runuser -l postgres -c "psql -c \"CREATE DATABASE $POSTGRES_DB\"" > /dev/null
runuser -l postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER\"" > /dev/null
if [ -f /data/schema.sql ]; then
    echo "      ⤷ Schema found and added to local db."
    runuser -l postgres -c "psql $POSTGRES_DB -f /data/schema.sql" > /dev/null
fi
echo "   Databases configured."

# Dump the schema from the current database.
runuser -l postgres -c "pg_dump --no-privileges --schema-only -f /tmp/schema.sql postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB"

filesize=$(stat -c%s /tmp/schema.sql)
if (( filesize > 0 )); then
    echo "   Schema generated."

    # Generate the delta difference between the schema and currently running database.
    migra --unsafe postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@localhost/$POSTGRES_DB > /tmp/_migrate.sql 2> /dev/null
    echo "   Delta migrations generated."

    # Chown & move the schema file to data.
    sudo chown "$USER":"$GROUP" /tmp/schema.sql /tmp/_migrate.sql


    echo
    echo "   Generation complete."
    # If the schema files are different, copy the generated file to the data directory.
    if ! diff /tmp/schema.sql /data/schema.sql > /dev/null; then
        echo "      ⤷ Schema updated and copied to directory."
        mv /tmp/schema.sql /data
    else
        echo "      ⤷ Schema not modified."
    fi

    # If the migrate file has anything in it, copy it to the data directory.
    filesize=$(stat -c%s /tmp/_migrate.sql)
    if (( filesize > 0 )); then
        echo "      ⤷ Migration file created and copied to directory."
        mv /tmp/_migrate.sql /data
    else
        echo "      ⤷ Migration file not required."
        rm /data/_migrate.sql > /dev/null 2> /dev/null
    fi

    echo
    echo "Schema generation complete."
    exit 0
else
    echo "   Schema could not be generated, please check database!"
    exit 1
fi